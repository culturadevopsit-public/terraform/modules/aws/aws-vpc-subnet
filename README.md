<!-- BEGIN_TF_DOCS -->



## Resources

The following resources are used by this module:

- [aws_subnet.subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) (resource)
- [aws_vpc.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) (resource)
```yaml
locals {
  vpc     = var.resources
  tags    = try(local.vpc.tags != null ? local.vpc.tags : null, null)
  subnets = local.vpc.subnets

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_vpc" "this" {
  cidr_block = local.vpc.cidr_block

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.vpc.name
    Description = "Created by ${var.project.createdBy}"
  })
}

resource "aws_subnet" "subnet" {
  for_each   = { for subnet in local.subnets : subnet.name => subnet }
  vpc_id     = aws_vpc.this.id
  cidr_block = each.value.cidr_block

  tags = merge(local.common_tags, try(each.value.tags != null ? each.value.tags : {
    Name        = each.value.name
    Description = "Created by ${var.project.createdBy}"
  }, []))
}
```

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws) (~>5.35.0)
```yaml
terraform {
  required_version = ">1.7.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}
```

## Required Inputs

The following input variables are required:

### <a name="input_project"></a> [project](#input\_project)

Description: some common settings like project name, environment, and tags for all resources-objects

Type:

```hcl
object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
```

### <a name="input_resources"></a> [resources](#input\_resources)

Description: resources of objects to create

Type:

```hcl
object({
    name       = string
    cidr_block = string
    tags       = optional(map(string))
    subnets = set(object({
      name       = string
      cidr_block = string
      tags       = optional(map(string))
    }))
  })
```

## Optional Inputs

No optional inputs.
```yaml
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    name       = string
    cidr_block = string
    tags       = optional(map(string))
    subnets = set(object({
      name       = string
      cidr_block = string
      tags       = optional(map(string))
    }))
  })
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
```

## Outputs

The following outputs are exported:

### <a name="output_debug"></a> [debug](#output\_debug)

Description: For debug purpose.

### <a name="output_subnets_arns"></a> [subnets\_arns](#output\_subnets\_arns)

Description: Arns of the created subnets of VPC.

### <a name="output_subnets_ids"></a> [subnets\_ids](#output\_subnets\_ids)

Description: Ids of the created subnets of VPC.

### <a name="output_vpc_arn"></a> [vpc\_arn](#output\_vpc\_arn)

Description: Arn of the created vpc.

### <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id)

Description: Id of the created vpc.
```yaml
output "vpc_id" {
  description = "Id of the created vpc."
  value       = aws_vpc.this.id
}

output "vpc_arn" {
  description = "Arn of the created vpc."
  value       = aws_vpc.this.arn
}

output "subnets_ids" {
  description = "Ids of the created subnets of VPC."
  value = [
    for subnet in aws_subnet.subnet : subnet.id
  ]
}

output "subnets_arns" {
  description = "Arns of the created subnets of VPC."
  value = [
    for subnet in aws_subnet.subnet : subnet.arn
  ]
}



output "debug" {
  description = "For debug purpose."
  value       = local.vpc
}
```

## Example from code .tf
```yaml
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name       = "vpc-1"
    cidr_block = "10.10.0.0/16",
    subnets = [
      {
        name       = "subnet-1"
        cidr_block = "10.10.1.0/24"
        tags = {
          Subnet = "Subnet-1"
        }
        }, {
        name       = "subnet-2"
        cidr_block = "10.10.2.0/24"
        tags = {
          Subnet = "Subnet-2"
        }
        }, {
        name       = "subnet-3"
        cidr_block = "10.10.3.0/24"
        tags = {
          Subnet = "Subnet-3"
        }
    }]
    tags = {
      Name = "vpc-1"
    }
  }

}
```
<!-- END_TF_DOCS -->