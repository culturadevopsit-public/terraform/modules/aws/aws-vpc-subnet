variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    name       = string
    cidr_block = string
    tags       = optional(map(string))
    subnets = set(object({
      name       = string
      cidr_block = string
      tags       = optional(map(string))
    }))
  })
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
